import {
  CHANGE_SCORE,
  SUMMARY
} from "./actionTypes";


export const handleScoreChange = (payload) => ({
  type: CHANGE_SCORE,
  payload,
});

export const handleSummary = (payload) => ({
  type: SUMMARY,
  payload,
});
