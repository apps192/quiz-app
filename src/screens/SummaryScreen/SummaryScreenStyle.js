import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({

    mainView:{
        marginHorizontal: 16,
        marginTop:20,
    },
    testSummary:{
        fontSize: 30,
        alignSelf: 'center'
    },
    questionTxt:{
        marginVertical:20,
        fontSize: 20,
    },
    optionTxt:{
        fontSize: 16,
    },
    scoreTxt:{
        fontSize: 24,
        marginTop: 10
    },
    unAnsweredTxt: {
        color: 'blue',
        fontSize: 24,
    },
    answeredView:{
        marginTop:10
    },
    buttonView:{
        height: 60,
        borderWidth: 3,
        alignItems:'center',
        justifyContent: 'center',
        paddingHorizontal: 20,
        marginVertical: 10,
    }
});

export { styles }