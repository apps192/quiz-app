export default data = [
    {
        question: "Q1. Select the correct statement about uncontrolled components.",
        options: [
            "It maintains its internal states",
            "A ref is used for their current values",
            "All",
            "data is controlled by the DOM itself"
        ],
        correct_option: "All",
        selected_option: ""
    },
    {
        question: "Q2. ________is used to create immutable stylesheet references",
        options: [
            "stylesheet",
            "Interaction manager",
            "Redux",
            "none"
        ],
        correct_option: "stylesheet",
        selected_option: ""
    },
    {
        question: "Q3. Which statement is true for props?",
        options: [
            "can be modified",
            "cannot be modified",
            "it is mutual",
            "it is set and updated by the object"
        ],
        correct_option: "cannot be modified",
        selected_option: ""
    },
    {
        question: "Q4. Virtual DOM creates a copy of the whole DOM object",
        options: [
            "TRUE",
            "FALSE"
        ],
        correct_option: "FALSE",
        selected_option: ""
    },
    {
        question: "Q5. React native was initially released in",
        options: [
            "2000",
            "2015",
            "2008",
            "2014"
        ],
        correct_option: "2015",
        selected_option: ""
    },
    {
        question: "Q6. To update react native with latest version what will you use?",
        options: [
            "none",
            "react-native init",
            "react-native upgrade",
            "both"
        ],
        correct_option: "both",
        selected_option: ""
    },
    {
        question: "Q7. Props are immutable. True or False?",
        options: [
            "TRUE",
            "FALSE"
        ],
        correct_option: "TRUE",
        selected_option: ""
    },
    {
        question: "Q8. JSX stands for",
        options: [
            "JointScript XML",
            "JavaScreenXML",
            "JavaScript XML",
            "none"
        ],
        correct_option: "JavaScript XML",
        selected_option: ""
    },
    {
        question: "Q9. Can we use React native for web?",
        options: [
            "NO",
            "YES"
        ],
        correct_option: "YES",
        selected_option: ""
    },
    {
        question: "Q10. Select apps which used react native",
        options: [
            "Airbnb",
            "Instagram",
            "pinterest",
            "All"
        ],
        correct_option: "All",
        selected_option: ""
    }
  ];
  