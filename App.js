import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import QuizScreen from './src/screens/QuizScreen';
import SummaryScreen from './src/screens/SummaryScreen';
import { Provider } from "react-redux";
import store from "./src/redux/store";

const Stack = createNativeStackNavigator();
const App = () => {
  return (
    <Provider store={store}>
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen
                    name="QuizScreen"
                    component={QuizScreen}
                    options={{headerShown: false}}
                    initialParams={{index: 0}}
                />
                <Stack.Screen
                    name="SummaryScreen"
                    component={SummaryScreen}
                    options={{headerShown: false}}
                />
            </Stack.Navigator>
        </NavigationContainer>
    </Provider>
  );
};

export default App;
