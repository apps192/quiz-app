import React from 'react';
import {
  View,
  Text,
  ScrollView
} from 'react-native';
import { styles } from "./SummaryScreenStyle";
import { useSelector } from "react-redux";

const SummaryScreen = () => {
    const {
        allQuestions,  
        score  
    } = useSelector((state) => state);
    const renderUnAnswered = () => {
        return (
            allQuestions?.map((item, index)=> {
                return(
                    <View key={index}>
                        {
                            item.selected_option == "" &&
                            <Text style={styles.questionTxt}>{item.question}</Text>
                        }
                    </View>
                )
            })
        )
    }
    const renderAnswered = () => {
        return (
            allQuestions?.map((item, index)=> {
                return(
                    <View key={index} style={styles.answeredView}>
                        {
                            item.selected_option !== "" &&
                            <Text style={styles.questionTxt}>{item.question}</Text>
                        }
                        {
                            item.selected_option !== "" &&
                            item.options.map((option, i)=> {
                                return(
                                    <View key={i} >
                                        <Text
                                            style={[styles.optionTxt, 
                                                {
                                                    backgroundColor:
                                                    option == item.correct_option && item.selected_option !== ""
                                                    ? 'green'
                                                    : option == item.selected_option && item.correct_option !== item.selected_option
                                                    ? 'red'
                                                    : 'transparent'
                                                }
                                            ]}
                                        >
                                            {option}
                                        </Text>
                                    </View>
                                )
                            })
                        }
                    </View>
                )
            })
        )
    }
    return (
        <View style={styles.mainView}>
            <ScrollView>
                <Text style={styles.testSummary}>Test Summary</Text>
                <Text
                    style={styles.scoreTxt}>
                    Score: {score}/10
                </Text>
                <Text style={styles.unAnsweredTxt}>UNANSWERED</Text>
                {/* UnAnswered Question */}
                {renderUnAnswered()}
                <Text style={styles.unAnsweredTxt}>ANSWERED</Text>
                {/* Answered Question */}
                {renderAnswered()}
            </ScrollView>
        </View>
    );
};
export default SummaryScreen;
