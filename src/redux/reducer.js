import {
  CHANGE_SCORE, 
  SUMMARY 
} from "./actionTypes";
import allQuestions from '../common/QuestionAnswer';

const initialState = {
  score: 0,
  allQuestions: allQuestions
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case CHANGE_SCORE:
        return {
            ...state,
            score: action.payload,
        };
        case SUMMARY:
        const questionAnswer = [...state.allQuestions];
        questionAnswer[action.payload.selectedIndex].selected_option = action.payload.selectedOption
        return {
            ...state,
            allQuestions: [...questionAnswer],
        };
        default:
        return state;
    }
};

export default reducer;
