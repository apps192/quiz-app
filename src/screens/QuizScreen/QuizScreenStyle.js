import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    mainView:{
        marginHorizontal: 16,
        marginTop:20
    },
    scoreTxt:{
        fontSize: 24,
    },
    questionTxt:{
        marginVertical:30,
        fontSize: 30,
    },
    scoreTimerView:{
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    optionTxt: {
        fontSize: 18,
    },
    buttonView:{
        height: 60,
        borderWidth: 3,
        alignItems:'center',
        justifyContent: 'center',
        paddingHorizontal: 20,
        marginVertical: 10,
    }
});

export { styles }