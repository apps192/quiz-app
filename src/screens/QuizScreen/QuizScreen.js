import React, {useEffect, useState, useRef} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
} from 'react-native';
import { useDispatch, useSelector } from "react-redux";
import { styles } from "./QuizScreenStyle";
import { handleScoreChange, handleSummary } from "../../redux/actions";

const QuizScreen = ({navigation}) => {
    const {
        score,  
        allQuestions  
    } = useSelector((state) => state);

    const dispatch = useDispatch();
    const [currentQuestionIndex, setCurrentQuestionIndex] = useState(0);
    const [currentOptionSelected, setCurrentOptionSelected] = useState(null);
    const [correctOption, setCorrectOption] = useState(null);
    const [isOptionsDisabled, setIsOptionsDisabled] = useState(false);
    const [counter, setCounter] = useState(120);
    
    let hours   = Math.floor(counter / 3600);
    let minutes = Math.floor((counter - (hours * 3600)) / 60);
    let seconds = counter - (hours * 3600) - (minutes * 60);

    useEffect(() => {
        const timer = counter > 0 && 
        setInterval(() => setCounter(counter - 1), 1000);
        if (counter == 0) {
            if (currentQuestionIndex !== allQuestions.length - 1) {
                setCurrentQuestionIndex(currentQuestionIndex + 1);
                setCounter(120)
            } else {
                navigation.navigate('SummaryScreen')
            } 
        }
        return () => clearInterval(timer);
    }, [counter, minutes, seconds]);

    // Set Answer
    const setAnswered = (selectedOption) => {
       return allQuestions[currentQuestionIndex].options.map((item, i) => {
            if(selectedOption === item)
            {
                //handleSummary
                dispatch(handleSummary({selectedOption: selectedOption, selectedIndex: currentQuestionIndex}))
            }
        })  
    }
    //Validate Answer
    const handleValidateAnswer = (selectedOption) => {
        let correct_option = allQuestions[currentQuestionIndex].correct_option;
        setCurrentOptionSelected(selectedOption);
        setCorrectOption(correct_option);
        setIsOptionsDisabled(true);

        // handle Score
        selectedOption == correct_option && dispatch(handleScoreChange(score + 1));
        setAnswered(selectedOption);

        // Go for next Question
        if (currentQuestionIndex !== allQuestions.length -1) {
            setTimeout(() => {
                setCurrentQuestionIndex(currentQuestionIndex + 1);
                setIsOptionsDisabled(false);
                setCounter(120)
            },2000)
        } else {
            setTimeout(() => {
                navigation.navigate('SummaryScreen')  
            },2000)
        }
    };
    const renderQuestion = () => {
        return (
        <View>
            {/* Question */}
            <Text 
                style={styles.questionTxt}>
                {allQuestions[currentQuestionIndex]?.question}
            </Text>
        </View>
        );
    };
    const renderOptions = () => {
        return (
        <View>
            {allQuestions[currentQuestionIndex]?.options.map(option => (
                
            <TouchableOpacity
                onPress={() => handleValidateAnswer(option)}
                disabled={isOptionsDisabled}
                key={option}
                style={[styles.buttonView, 
                    {
                        borderColor:
                        currentOptionSelected == correctOption && option == correctOption
                        ? 'green'
                        : currentOptionSelected != correctOption && currentOptionSelected == option
                        ? 'red'
                        : 'black',
                        backgroundColor:
                        currentOptionSelected == correctOption && option == correctOption
                        ? 'green'
                        : currentOptionSelected != correctOption && currentOptionSelected == option
                        ? 'red'
                        : 'transparent'
                    }
                ]}
            >
                <Text style={styles.optionTxt}>{option}</Text>
            </TouchableOpacity>
            ))}
        </View>
        );
    };
    return (
        <View style={styles.mainView}>
            <View style={styles.scoreTimerView}>
                {/* Score */}
                <Text style={styles.scoreTxt}>
                    Score: {score}/10
                </Text>
                <Text style={styles.scoreTxt}>{minutes}:{seconds}</Text>
            </View>
            {/* Question */}
            {renderQuestion()}
            {/* Options */}
            {renderOptions()}
        </View>
    );
};

export default QuizScreen;
